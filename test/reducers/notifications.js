import expect from 'expect';
import reducer from '../../app/src/reducers/_inc/notifications';
import { NOTIFY_MESSAGE } from '../../app/src/actions/notifications';

describe('notifications reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual({})
  });

  it('NOTIFY_MESSAGE should return an updated state containing new notification', () => {
    expect(reducer({}, {
    type: NOTIFY_MESSAGE,
      payload: {
        title: 'Run Test',
        message: 'Test ran successfully',
        level: 'success'
      }
    })).toEqual(
        {
          title: 'Run Test',
          message: 'Test ran successfully',
          level: 'success'
        }
      )
  });
});
