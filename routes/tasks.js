var express = require('express');
var router = express.Router();
var repos = require('../repos');
var Promise = require("bluebird");
var ResponseUtils = require('../utils/response-utils.js');

/* Routes */

router.get('/', function(req, res, next) {
  repos.tasks.list().then(ResponseUtils.json(res), next);
})

.post('/', function(req, res, next) {
  repos.tasks.post(req.body).then(function (newId) {
    res
      .status(201)
      .location('/v1/tasks/' + newId)
      .json(newId);
  }, next);
})

.get('/:id', function(req, res, next) {
  repos.tasks.get(req.params.id).then(ResponseUtils.json(res), next);
})

.patch('/:id', function(req, res, next) {
  console.log(req.body);
  repos.tasks.patch(req.params.id, req.body).then(ResponseUtils.json(res), next);
})

.delete('/:id', function(req, res, next) {
  repos.tasks.delete(req.params.id).then(ResponseUtils.noContent(res), next);
});

module.exports = router;
