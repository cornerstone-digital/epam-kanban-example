export const NOTIFY_MESSAGE = 'notify/message';

export function showNotification(notification) {
  return {
    type: NOTIFY_MESSAGE,
    payload: notification,
  };
}
