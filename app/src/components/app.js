import React from 'react';
import { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import NotificationSystem from 'react-notification-system';
import { fetchStories } from '../actions/stories';
import { fetchTasks } from '../actions/tasks';

export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    notification: PropTypes.object.isRequired,
    stories: PropTypes.array.isRequired,
    tasks: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);

    this.props.fetchStories();
    this.props.fetchTasks();
  }

  componentDidMount() {
    this.notificationSystem = this.refs.notificationSystem;
  }

  componentWillUpdate(nextProps) {
    if (this.props.notification !== nextProps.notification) {
      this.notificationSystem.addNotification(nextProps.notification);
    }
  }

  render() {
    return (
      <div className="wrapper">
        {this.props.children}

        <NotificationSystem ref="notificationSystem" />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { notification: state.notification, stories: state.stories, tasks: state.tasks };
}

export default connect(mapStateToProps, { fetchStories, fetchTasks })(App);
