import React, { Component } from 'react';
import TaskCard from './TaskCard';
import { Link } from 'react-router';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { deleteStory } from '../../../../actions/stories';

class StoryRow extends Component {
  constructor(props) {
    super(props);
  }

  filterTasksByStatus(tasks, status) {
    return tasks.filter(task => task.status === status);
  }

  deleteStory() {
    this.props.deleteStory(this.props.story.id);
  }

  render() {
    const statuses = [
      'to do', 'in progress', 'done'
    ];

    return (
      <tr className="storyRow">
        <td valign="top">
          <p className="story_id">ID: {this.props.story.id}</p>
          <p className="story_feature">{this.props.story.feature}</p>
          <p className="story_priority">Priority: {this.props.story.priority}</p>

          <Link to={'/story/' + this.props.story.id} className="btn btn-primary pull-left">Edit</Link>
          <Button className="btn btn-danger pull-left" onClick={this.deleteStory.bind(this)} disabled={this.props.tasks.length > 0}>Delete</Button>
          <Link to={'/story/' + this.props.story.id + '/tasks/add/'} className="btn btn-primary pull-right">Add Task</Link>
        </td>
        {statuses.map((status) => (
          <td key={status}>
            {this.filterTasksByStatus(this.props.tasks, status).map((task) => (
              <TaskCard key={task.id} story_id={this.props.story.id} task={task} />
            ))}
          </td>
        ))}
      </tr>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, { deleteStory })(StoryRow);
