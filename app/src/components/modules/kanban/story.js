import React, { Component } from 'react';
import { Input, ButtonInput } from 'react-bootstrap';
import { connect } from 'react-redux';
import { addStory, updateStory } from '../../../actions/stories';
import KanbanService from '../../../services/KanbanService';

class Story extends Component {
  static contextTypes = {
    router: React.PropTypes.object,
    store: React.PropTypes.object
  };

  state = {
    canSubmit: false,
    buttonValue: 'Add Story',
    story: {
      priority: 1,
      persona: 'Admin',
      feature: '',
      justification: ''
    }
  };

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    if(this.props.params.storyId) {
      this.setState({
        buttonValue: 'Save Story',
        story: this.context.store.getState().stories.filter(story => story.id === this.props.params.storyId)[0]
      }, () => {
        this.validateForm();
      });
    }

    this.validateForm();
  }

  onPriorityChange(e) {
    const value = e.target.value;

    this.setState({
      story: {
        ...this.state.story,
        priority: value
      }
    }, () => {
      this.validateForm();
    });
  }

  onPersonaChange(e) {
    e.preventDefault();
    const value = e.target.value;

    this.setState({
      story: {
        ...this.state.story,
        persona: value
      }
    }, () => {
      this.validateForm();
    });
  }

  onFeatureChange(e) {
    const value = e.target.value;

    this.setState({
      story: {
        ...this.state.story,
        feature: value
      }
    }, () => {
      this.validateForm();
    });
  }

  onJustificationChange(e) {
    const value = e.target.value;

    this.setState({
      story: {
        ...this.state.story,
        justification: value
      }
    }, () => {
      this.validateForm();
    });
  }

  validateForm() {
    let valid = true;

    if(this.state.story.priority.length === 0) {
      valid = false;
    }

    if(this.state.story.persona.length === 0) {
      valid = false;
    }

    if(this.state.story.feature.length === 0) {
      valid = false;
    }

    if(this.state.story.justification.length === 0) {
      valid = false;
    }

    if(valid) {
      this.enableButton();
    }
    else {
      this.disableButton();
    }
  }

  disableButton() {
    this.setState({
      canSubmit: false
    });
  }

  enableButton() {
    this.setState({
      canSubmit: true
    });
  }

  submit(e) {
    e.preventDefault();

    if(this.props.params.storyId) {
      this.props.updateStory(this.state.story);

      this.context.router.push('/');
    } else {
      KanbanService.addStory(this.state.story)
        .then(storyId => {
          return KanbanService.getStoryById(storyId);
        })
        .then(story => {
          this.props.addStory(story);

          this.context.router.push('/');
        });
    }
  }

  render() {
    var priorities = [
      {value: '1', label: 'Must Have'},
      {value: '2', label: 'Nice To Have'},
      {value: '3', label: 'Low Priority'}
    ];
    return (
      <section className="story">
        <h1>Story</h1>
        <form onSubmit={this.submit.bind(this)}>
          <Input name="priority" ref="priority" type="select" label="Priority" value={this.state.story.priority} onChange={this.onPriorityChange.bind(this)} required>
            {priorities.map((priority, index) => (
              <option key={index} value={priority.value}>{priority.value}: {priority.label}</option>
            ))}
          </Input>
          <Input name="persona" ref="persona" type="text" label="Persona" value={this.state.story.persona} onChange={this.onPersonaChange.bind(this)} required/>
          <Input name="feature" ref="feature" type="textarea" label="Feature" value={this.state.story.feature} onChange={this.onFeatureChange.bind(this)} required/>
          <Input name="justification" ref="justification" type="text" label="Justification" value={this.state.story.justification} onChange={this.onJustificationChange.bind(this)} required/>
          <ButtonInput type="submit" value={this.state.buttonValue} disabled={!this.state.canSubmit} />
        </form>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, { addStory, updateStory })(Story);
