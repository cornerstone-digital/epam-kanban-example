export default {
  api: {
    baseURL: 'http://localhost:3000/',
    apiVersion: 'v1',
  }
};

if (process.env.NODE_ENV === 'production') {
  // Set Production Values
}
