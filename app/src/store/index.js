import { createStore, applyMiddleware, compose } from 'redux';
import reducer from '../reducers';
import Async from '../middlewares/async';
//import Api from '../middlewares/api';
import createLogger from 'redux-logger';

export default function configureStore(initialState) {
  const finalCreateStore = compose(
    applyMiddleware(Async, createLogger()),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )(createStore);

  const store = finalCreateStore(reducer, initialState);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers');
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
