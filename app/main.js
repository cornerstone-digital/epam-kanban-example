import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import App from 'components/app';

// Higher Order Components
import layoutHandler from './src/components/hoc/layout_handler';

// Layouts
import MainLayout from 'components/layouts/main/mainLayout';

// Pages
import Board from './src/components/modules/kanban/board';
import Story from './src/components/modules/kanban/story';
import Task from './src/components/modules/kanban/task';

import configureStore from './src/store';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <div>
      <Router history={browserHistory}>
        <Route path="/" component={App}>
          <IndexRoute component={layoutHandler(Board, MainLayout)}/>
          <Route path="/story/add" component={layoutHandler(Story, MainLayout)} />
          <Route path="/story/:storyId" component={layoutHandler(Story, MainLayout)} />
          <Route path="/story/:storyId/tasks/add" component={layoutHandler(Task, MainLayout)} />
          <Route path="/tasks/:taskId" component={layoutHandler(Task, MainLayout)} />
        </Route>
      </Router>
    </div>
  </Provider>
  , document.querySelector('.container'));
