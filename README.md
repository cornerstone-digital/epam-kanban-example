# Front-end recruitment challenge: Agile board

Agile Board solution built using ReactJS and Redux (Flux) for state management.

**Applicant: Martin Egan**

## Building, testing and running

To build, test and run the application use node package manager.

**Build**
```
npm install
```

**Test**
```
npm test
```

**Run**
```
npm start
```
